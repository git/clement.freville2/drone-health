<!DOCTYPE html>
<html lang="en">
<head>
  <title>drone-health - One incredible styled html page</title>
  <link rel="stylesheet" href="/mycoolstyle.css">
</head>

<body>

<nav>
<ul class="navbar">
  <li><a href="https://codefirst.iut.uca.fr/home">Code#0</a>
  <li><a href="http://iut.uca.fr">IUT</a>
  <li><a href="https://neilyoungarchives.com/">Very good music</a>
  <li><a href="https://www.youtube.com/watch?v=CY5dTBhRxOA">Very good music too!</a>
</ul>
</nav>

<section>
<h1><?= $_SERVER['REQUEST_URI'] ?> is my 99th page, and it has got style!</h1>

<p>Welcome on this template html/css project</p>

<p>Very simple, some links, some menu... make it your own</p>

<p>I have nothing more to say</p>
</section>

<footer>
<address>Template made in 2022<br>
  with Code#0 <a href="https://codefirst.iut.uca.fr/home"><img src="/images/CodeFirstLogo.png" alt="Code#0" height="40px"/>
  </a></address>
</footer>
<script>
// If current url does not end with a slash, add it
if (!window.location.href.endsWith('/')) {
  window.location.href = window.location.href + '/';
}
</script>
</body>
</html>